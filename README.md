![](https://elioway.gitlab.io/eliosin/god/elio-god-logo.png)

> Anything this easy must be sinful **the elioWay**

# god ![alpha](https://elioway.gitlab.io/eliosin/icon/devops/alpha/favicon.ico "alpha")

A _pattern_ for creating HTML wireframes **the elioWay**. No-effort, off-the-grid, auto-scaling HTML wireframes in SASS with no class and a light footprint. Now with extra fibonacciness.

- [god Documentation](https://elioway.gitlab.io/eliosin/god)
- [god Demo](https://elioway.gitlab.io/eliosin/god/demo.html)

## Installing

```shell
npm install @elioway/god --save
yarn add  @elioway/god --dev
```

- [Installing god](https://elioway.gitlab.io/eliosin/god/installing.html)

## Seeing is Believing

```shell
git clone https://gitlab.com/eliosin/god.git
cd god
npm i|yarn
gulp
```

## Nutshell

### `gulp`

### `npm test`

### `npm run prettier`

- [god Quickstart](https://elioway.gitlab.io/eliosin/god/quickstart.html)
- [god Credits](https://elioway.gitlab.io/eliosin/god/credits.html)
- [god Issues](https://elioway.gitlab.io/eliosin/god/issues.html)

![](https://elioway.gitlab.io/eliosin/god/apple-touch-icon.png)

## License

[HTML5 Boilerplate](LICENSE.txt) [Tim Bushell](mailto:theElioWay@gmail.com)

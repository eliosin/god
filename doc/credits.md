# god Credits

## Core, ta thanks!

- [scss](https://scss.com)
- [gulp](https://gulpjs.com)
- [Hugo "Kitty" Giraudel](https://css-tricks.com/snippets/sass/strip-unit-function/)

## Artwork

- [wikimedia:Ermitorio_del_Calvario_5](https://commons.wikimedia.org/wiki/File:Ermitorio_del_Calvario_5.jpg)
- [wikimedia:The_Flood](<https://commons.wikimedia.org/wiki/File:The_Flood>_(Die_Sintflut,\_Suendflut)\_by_Lesser_Ury_(3561574020).jpg>)
- [wikimedia:Earth_and_Paint](https://commons.wikimedia.org/wiki/File:"Earth_and_Paint"_by_Zac_Dutton.jpg)
- [wikimedia:Pillars_of_creation](https://commons.wikimedia.org/wiki/File:Pillars_of_creation_2014_HST_WFC3-UVIS_full-res_denoised.jpg)
- [unsplash:LightSwitch](https://unsplash.com/photos/ynfmVbvunQU)

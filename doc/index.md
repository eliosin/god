<aside>
  <dl>
  <dd>To thee I have transferred</dd>
  <dd>All judgement, whether in Heaven, or Earth, or Hell.</dd>
</dl>
</aside>

**god** generates a tagName-only CSS stylesheet which lays out your standard HTML into a flexible wireframe made of three _prongs_.

Since we're doing things **the elioWay** - as discussed in [eliosin](/eliosin) - our attention is now focused on tagNames, not classNames.

**god**'s job is to style the layout of the tagNames you have chosen to be part of your eliosin theme.

<div><a href="demo.html" target="_demo">
  <button>Demo</button>
</a>
  <a href="mixins.html">
  <button>Mixins</button>
</a>
  <a href="settings.html">
  <button>Settings</button>
</a></div>

# Prongology

Behind the scenes, **god** divides the pages into three columns, which we call _prongs_.

- the _prong_ on the left is called _hell_
- the _prong_ in the centre is called _pillar_
- the _prong_ on the right is called _heaven_

In **god**'s settings file, you choose which tagNames go into which _prong_. Any tagNames "pronged" (i.e. mention in **god**'s settings file) will be known as _believers_.

You write your HTML with no class names, and with your _believers_ written as siblings of a parent HTML element, called the _container_; preferably `body` or `body>main`.

**god** positions your content in the chosen _prong_ and does all the responsive stuff web developers need for mobile development.

<address>
  I'm in heaven.
</address>

<aside>
  <strong>I'm in hell.</strong>
</aside>

<article>
  <strong>I'm in the pillar.</strong>
</article>

# Nutshell

**god** - in principle - works in elioWays:

- tagNames are selectors, not classNames.
- In a setting file, you list some tags in the _pillar_, some in _hell_, some in _heaven_
- **god** writes you a css stylesheet which lays out ...

  - ... all the tags in the pillar down the centre of the page
  - ... all the tags in hell hanging on the left side of the page
  - ... all the tags in heaven hanging on the right side of the page

# Dogma

1. **god** shall only be used for page layout. _Style your own forms_.
2. **god** layouts shall target tagNames not classNames.
3. _pillar_ _hell_ _heaven_ shall be siblings of the same _container_.
4. _pillar_ tags shall be laid-out as an equal-width, centred column.
5. _hell_ tags shall be laid-out left and _heaven_ tags shall be laid-out right.
6. No _hell_ _heaven_ shall be laid-out higher on the page than the _following_ sibling _pillar_ in the HTML source (examples coming).
7. _pillar_ content shall wrap when overlapping _hell_ _heaven_.

_Terms and conditions apply. **god** retains the right to change these rules at a whim._

[eliosin Dogma](/eliosin/dogma.html)

# eliosin Mixins

A list of **god**'s mixins.

## `all_are_judged`

Master styling for believers.

```scss
/*
@param {%} $w width
@param {UNIT} $pv padding-vertically
@param {%} $pl padding-horizontally
*/
```

### Example

```scss
body > $pillar {
  @include all_are_judged(40%, 4px, 3%);
}
```

### Outputs

```scss
body > h1,
body > h2,
body > p {
  box-sizing: border-box !important;
  display: inline-block;
  margin: 0 0 4px !important;
  padding: 4px 3% !important;
  width: 40% !important;
}
```

## `colorize_god`

Utility to add background and foreground color to any tag.

```scss
/*
@param {STR} $bgc background-color
@param {STR} $c color
@param {STR} $bc border-color
*/
```

### Example

```scss
body > $pillar {
  @include colorize_god(red, white, blue solid 1px);
}
```

### Outputs

```scss
body > h1 a,
body > h2 a,
body > p a {
  background-color: red;
  border: blue solid 1px;
  color: white;
}
```

## `colorize_link`

Utility to create link colors from one primary color.

```scss
/*
@param {STR} $c color
*/
```

### Example

```scss
body > $pillar {
  @include colorize_link(red);
}
```

### Outputs

```scss
body > h1 a,
body > h2 a,
body > p a {
  color: #990000 !important;
  &:visited {
    color: #990000 !important;
  }

  &:hover {
    color: red !important;
  }

  &:active {
    color: cyan !important;
  }
}
```

## `condemn_sinner`

Master style for tags in hell.

```scss
/*
@param {%} $w width
@param {UNIT} $pv padding-vertically
@param {%} $ph padding-horizontally
*/
```

### Example

```scss
body > $pillar {
  @include condemn_sinner(40%, 4px, 3%);
}
```

### Outputs

```scss
body > h1,
body > h2,
body > p {
  padding: 4px 3% 0 3% !important;
  width: 40% - (2% + 2%) !important;
  margin-top: 0 !important;
  margin-bottom: 1% !important;
  margin-left: 0 !important;
  margin-right: 0 !important;
  break-before: always;
  display: inline-block;
  clear: left;
  float: left;
  text-align: left;
}
```

## `cosy`

Media widths for medium screen. Uses media-width settings.

### Example

```scss
body {
  @include cosy {
    font-size: 100%;
  }
}
```

### Outputs

```scss
body {
  @media screen and (min-width: 900px + 1px) and (max-width: 1200px) {
    font-size: 100%;
  }
}
```

## `debug_show`

Makes an element stand out on the page for debugging.

### Example

```scss
body > $pillar {
  @include debug_show());
}
```

### Outputs

```scss
body > h1,
body > h2,
body > p {
  background-color: red;
  color: white;
  border: blue solid 1px;
}
```

## `fixed_height`

Fix a tag height.

```
/*
@param {UNIT} $sz fixed height
*/
```

### Example

```scss
body > .adonToolBar {
  @include fixed_height(123px);
}
```

### Outputs

```scss
body > h1,
body > h2,
body > p {
  height: 123px !important;
  min-height: 123px !important;
  max-height: 123px !important;
}
```

## `fixed_width`

Fix a tag height.

```
/*
@param {UNIT} $sz fixed width
*/
```

### Example

```scss
body > .adonToolBar {
  @include fixed_width(123px);
}
```

### Outputs

```scss
body > h1,
body > h2,
body > p {
  width: 123px !important;
  min-width: 123px !important;
  max-width: 123px !important;
}
```

## `forgive_sinner`

Master style for tags in heaven.

```scss
/*
@param {%} $w width
@param {UNIT} $pv padding-vertically
@param {%} $ph padding-horizontally
@param {%} $m margin
@param {UNIT} $b border
*/
```

### Example

```scss
body > $pillar {
  @include forgive_sinner(40%, 2%, 1%);
}
```

### Outputs

```scss
body > h1,
body > h2,
body > p {
  padding: 6px 2% 0 2% !important;
  width: 40% - (2% + 2%) !important;
  margin-top: 0 !important;
  margin-bottom: 1% !important;
  margin-left: 0 !important;
  margin-right: 0 !important;
  break-before: always;
  break-before: always !important;
  clear: right;
  float: right;
  text-align: right;
}
```

## `full`

Media widths for full screen. Uses media-width settings.

### Example

```scss
body {
  @include full {
    font-size: 100%;
  }
}
```

### Outputs

```scss
body {
  @media screen and (min-width: 1600px) {
    font-size: 100%;
  }
}
```

## `heading_sizes`

Master style for heading spreads

```scss
/*
@param {LIST} $fontzy A list of 6 heading sizes
*/
```

### Example

```scss
body > {
  @include heading_sizes(600% 500% 400% 300% 200% 100%);
}
```

### Outputs

```scss
body p {
  h1 {
    font-size: 600%;
  }
  h2 {
    font-size: 500%;
  }
  h3 {
    font-size: 400%;
  }
  h4 {
    font-size: 300%;
  }
  h5 {
    font-size: 200%;
  }
  h6 {
    font-size: 100%;
  }
}
```

## `loose`

Media widths for large screen. Uses media-width settings.

### Example

```scss
body {
  @include loose {
    font-size: 100%;
  }
}
```

### Outputs

```scss
body {
  @media screen and (min-width: 600px + 1px) and (max-width: 90px) {
    font-size: 100%;
  }
}
```

## `pillar_border`

Padding style to contain a pillar.

Apply vertical padding as the margin between tags, and horizontal padding to pad between the pillar and the believers.

```scss
/*
@param {UNIT} $pv padding-top padding-bottom
@param {%} $ph padding-left padding-right
*/
```

### Example

```scss
body > $pillar {
  @include pillar_border(false, green solid 1px);
}
```

### Outputs

```scss
body > h1,
body > h2,
body > p {
  border-right: green solid 1px !important;
}
```

## `pillar_containment`

Margin style to contain a pillar.

Using left and right margins as percentages to fix the column, with no vertical margin.

```scss
/*
@param {%} $ml margin-left
@param {%} $mr margin-right
*/
```

### Example

```scss
body > $pillar {
  @include pillar_containment(30%, 30%);
}
```

### Outputs

```scss
body > h1,
body > h2,
body > p {
  margin: 0 30% 0 30% !important;
}
```

## `pillar_padding`

Padding style to contain a pillar.

Apply vertical padding as the margin between tags, and horizontal padding to pad between the pillar and the believers.

```scss
/*
@param {UNIT} $pv padding-vertically
@param {%} $ph padding-horizontally
*/
```

### Example

```scss
body > $pillar {
  @include pillar_padding(0, 3%);
}
```

### Outputs

```scss
body > h1,
body > h2,
body > p {
  padding: 0 3% !important;
}
```

## `pillar_toolbar`

Convert a pillar to a toolbar. Toolbars are created by js.

```scss
/*
@param {UNIT} $pv padding-vertically
@param {%} $ph padding-horizontally
*/
```

### Example

```scss
body > $pillar {
  @include pillar_toolbar(4px, 3%);
}
```

### Outputs

```scss
body > h1,
body > h2,
body > p {
  max-height: 32px !important;
  text-align: right !important;
  padding-right: 3% !important;
  padding-top: 4px !important;
  font-size: 100% !important;
  margin-bottom: 0 !important;
}
```

## `pillaraged`

A column of tags forming a vertical pillar on the web page.

```scss
/*
@param {%} $ml margin-left
@param {%} $mr margin-right
@param {UNIT} $pv padding-vertically
@param {%} $ph padding-horizontally
*/
```

### Example

```scss
body > $pillar {
  @include pillaraged(20%, 40%, 4px, 3%);
}
```

### Outputs

```scss
body > h1,
body > h2,
body > p {
  margin: 0 20% 0 40% !important;
  > ol,
  > ul {
    padding-left: 3%;
    padding-right: 3%;
  }
  padding: 4px 3% !important;
  display: block;
  float: none;
}
```

## `polite`

Utility to zero the border, margin and padding of a believer.

```scss
/*
@param {STR} $sz unit size
*/
```

### Example

```scss
h1 {
  @include polite();
}
```

### Outputs

```scss
h1 {
  border: none !important;
  margin: 0;
  padding: 0;
}
```

## `sinner_toolbar`

Style a toolbar for any believer. Toolbars are created by js.

```scss
/*
@param {UNIT} $pv padding-vertically
@param {%} $ph padding-horizontally
*/
```

### Example

```scss
body > $hell {
  @include sinner_toolbar(4px, 3%);
}
```

### Outputs

```scss
body > nav,
body > menu {
  font-size: 100% !important;
  margin-bottom: 0 !important;
  max-height: 36px !important;
  max-width: 36px !important;
  padding: 4px 3% !important;
  text-align: center !important;
}
```

## `tight`

Media widths for small screen. Uses media-width settings.

### Example

```scss
body {
  @include tight {
    font-size: 100%;
  }
}
```

### Outputs

```scss
body {
  @media screen and (max-width: 600px) {
    font-size: 100%;
  }
}
```

## `tight_and_cosy`

Media widths for small to medium screen. Uses media-width settings.

### Example

```scss
body {
  @include tight_and_cosy {
    font-size: 100%;
  }
}
```

### Outputs

```scss
body {
  @media screen and (min-width: 600px + 1px) and (max-width: 1200px) {
    font-size: 100%;
  }
}
```

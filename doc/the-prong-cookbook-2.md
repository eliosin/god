# The Prong Cookbook 2: Using classes

## The issue:

The problem with eliosin is that once you have committed a tagName to _hell_ or _heaven_ it can only appear in that column.

I say problem. It's only a problem if you want to use the same tagName in more than one _prong_.

One solution is [toothpaste](/eliosin/toothpaste) which alternates. This can work great, and hits the main goal of **eliosin**: "take care of this for me".

But it still doesn't allow you the flexibility.

## The solution:

The solution is very simple. While **god** is a classless, wireframe CSS framework, it's also part of **eliosin**; and in **eliosin** it's okay to sin. You can use classes.

## Example settings file

```shell
$container: (
  article,
);
$pillar: (
  '.pillar', h1, h2, h3, ul, ol, dl, p, # are in the pillar
);
$heaven: (
  '.heaven', header, blockquote, h4, # are in heaven
);
$hell: (
  '.hell', menu, nav, aside, # are in hell
);
```

Now you can add the class `pillar`, `heaven` or `hell` to any tagName and this will override its position.

# Installing god

- [god Prerequisites](/eliosin/god/prerequisites.html)

## Installing with generator-sin

Installing with eliosin's own **generator-sin** is the recommended way.

- [Installing generator-sin](/eliosin/generator-sin/installing.html)

## Installing with NPM or Yarn as a dependency of your current project

Add **god** as an NPM package.

```shell
npm install @elioway/god --save-dev
yarn add @elioway/god --dev
```

### Add it to your compile funnel

- Locate the primary `X.scss` build file within your current project stylesheets.

  - For instance, the one targeted by your compile tools in the `gruntfile`, `gulpfile` or other.

- Add the following to your current project SASS build file:

```scss
@import "../node_modules/@elioway/god/stylesheets/settings";
@import "../node_modules/@elioway/god/stylesheets/theme";
```

### Using your own settings

- Copy the `settings.scss` file from `./node_modules/@elioway/god/stylesheets/settings.scss` -

- Paste it alongside `X.scss` into, for instance, `my_gods_settings.scss`

- Change the SASS build file making sure your settings gets called before **god**'s theme:

```scss
@import "my_gods_settings";
@import "../node_modules/@elioway/god/stylesheets/theme";
```

### Nah! I'll just use the dist

```html
<link rel="stylesheet" href="node_modules/@elioway/god/dist/css/god.min.css" />
```

# gulp watch issues

If gulp crashes while running "watch", try this shell command in Linux.

```shell
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf
sudo sysctl -p
```
